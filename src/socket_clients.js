/*
	Demonstrating how clients connect and exchange messages using Sockets
*/
 
const request = require('./request.js');
const io = require("socket.io-client");
const sleep = require('sleep-promise');

 
// With running user gateway and external storage locally
//const gatewayApiHost = 'http://localhost:3000/'; 
 
// With staging environment
const gatewayApiHost = 'https://staging.gateway.schluss.app/';

async function run(){

	// Register client A at user gateway
	let clientA = JSON.parse(await request.post(gatewayApiHost + 'register'));

	console.log('Client A registration result from User Gateway:');
	console.log(clientA);

	// Initiate socket communication channel with token for client A
	const socketClientA = io(gatewayApiHost,  {  autoConnect: false  });
	socketClientA.auth = { token : clientA.key, secret : clientA.secret};
	socketClientA.connect();
	
	// called when Client A recieves a message
	socketClientA.on('message', function (data) {
		console.log('Message to Client A: ' + data);
	});	
	
	// when errors occur on Client a
	socketClientA.on('connect_error', (err) => {
	  console.log(err.message);
	});	
	
	socketClientA.on('error', (err) => {
	  console.log(err.message);
	});	



	// Let's register another client: client B

	// Register client B at user gateway
	let clientB = JSON.parse(await request.post(gatewayApiHost + 'register'));

	console.log('Client B registration result from User Gateway:');
	console.log(clientB);
	
	// initiate socket communication channel with token for client B
	const socketClientB = io(gatewayApiHost,  {  autoConnect: false  });
	socketClientB.auth = { token : clientB.key, secret : clientB.secret};
	socketClientB.connect();
	
	// called when Client A recieves a message
	socketClientB.on('message', function (data) {
		console.log('Message to Client B: ' + data);
	});	
	
	// when errors occur on Client a
	socketClientB.on('connect_error', (err) => {
	  console.log(err.message);
	});	
	
	socketClientB.on('error', (err) => {
	  console.log(err.message);
	});	
	
	// Let's send some messages to the clients!
	await sleep(1000);	// some delay for this demo to make sure all the above is initiated

	
	// To send a message from Client B to client A, you need to have clientA.key (this must be shared at forehand)
	socketClientB.emit('message', {
		to : clientA.key,
		message : 'Hi client A, this is a message from client B (token ' + clientB.key + ')'
	});
	
	// To send a message from outside to client A, you need to have clientA.key (this must be shared at forehand)
	request.post(gatewayApiHost + 'messages', {
		body : {
			to : clientA.key,
			message : 'Hi! I am a message from an external source, send using a regular POST request!'
		},
		json:true
	});	
	
	// Note: message can be anything, also a nested object structure
}


run();
