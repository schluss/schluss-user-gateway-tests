/*
	Demonstrating the gateway api flow
*/
 
const request = require('./request.js');
const sleep = require('sleep-promise');
const openpgp = require('openpgp');
const aes256 = require('aes256');
 
// With running user gateway and external storage locally
//const storageApiHost = 'http://localhost:3001/';
//const gatewayApiHost = 'http://localhost:3000/'; 
 
// With staging environment
const storageApiHost = 'https://staging.storage.schluss.app/';
const gatewayApiHost = 'https://staging.gateway.schluss.app/';



async function run(){
 
	output.message('Test user flow');
	
	await output.wait(2);
	
	// 1. PREQUISITES
	
		output.message('First, user and data requester need a keypair');
		await output.wait(2);
		output.message('Generating...');
		
		const requester = await setupDataRequester();
		
		const user = await setupSchlussApp();

		output.status('ok');
		await output.wait(2);
	
	// 2. USER REGISTERS AT STORAGE PROVIDER
	// In this example it's done programatically. You could actually compare this with creating a Dropbox account
	
		output.message('User creates an account on an external storage provider, like Solid/Dropbox/Amazon S3 etc\n\n');
		await output.wait(1);
		output.message('Registering at External storage provider...');
		
		const registerResult = await registerExternalStorage();
		
		// returns:
		// registerResult.key 		(which is like a username in this demo)
		// registerResult.secret	(which is like a password in this demo)

		output.status('ok');
		await output.wait(2);
		
	// 3. USER AUTHORIZES SCHLUSS APP AT STORAGE PROVIDER
	// Normally this is actually an oAuth / OpenID Connect flow where the Schlus app is authorized and recieves a token which gives access 
	
		output.message('\n\nAuthorize Schluss Users \'app\' at Ext storageprovider...');
		await output.wait(1);
		output.message('Authorizing...');
		
		const authResult = await authorizeSchlussAppExternalStorage();
	
		// returns:
		// authResult.token (session token) 
		
		output.status('ok');
		await output.wait(2);
	
	// 4. USER DOES A DATAREQUEST AND STORES THE ATTRIBUTE WITHIN THE APP 
	
		let appStorage = await getAndStoreAttributesLocally(user, requester);
	

		await output.wait(2);
	
	// 5. APP STORES THE ATTRIBUTE AND METADATA ON EXTERNAL STORAGE
	
	
		output.message('\n\nNow the attribute with its metadata is going to be also stored online');
		
		const attrValueStoreLocation = await storeAttributesExternalStorage(user, appStorage, authResult.token);
		
		
		await output.wait(2);
		

	// 6. APP CREATES A 'SHARE' LINK ON THE USER GATEWAY
	
		await output.wait(1);
		output.message('\n\nNow, instead of sending the data (attribute Value), we are going to create a link using our Gateway which we share to the data requester');
		
		const gatewayResult = await createUserGatewayLink(attrValueStoreLocation, authResult.token);
		

	
	// 7. USER/APP FINISHES DATAREQUEST AND GETWAY LINK IS SHARED WITH DATA REQUESTER
	// ... this is omitted in this example
	
		await output.wait(1);
		output.message('\n\nFinally, the returned location and readToken is shared to the datarequester. And the datarequester is now going to access the attribute via the Gateway.\n');
		await output.wait(4);	
		
	// 8. DATAREQUESTER IS NOW GOING TO ACCESS THE ATTRIBUTE THROUGH THE GATEWAY LINK
	
		await datarequesterGetDataFromUserGateway(gatewayResult, requester);
		
		await output.wait(1);

	
	// 9. USER LOOKS UP ACCESS LOG ON THE OBJECT
	// this gives the user control, because he/she is able to see when the attribute is accessed and for what purpose
	
		await userGetsAccessLog(gatewayResult);
		
		await output.wait(1);
		output.message("\n\nDone\n\n");	
	
	
	// cleanup here?
}




async function setupDataRequester(){
	
	// Third party also has a keypair
    const requesterKey = await openpgp.generateKey({
        type: 'ecc', 
        curve: 'curve25519', 
        userIds: [{ name: 'x', email: 'x@x.com' }], 
        passphrase: 'providerpassword'
    });	
	// also get the public key fingerprint of the data requester
	const tmpRequesterPublicKey = await openpgp.readKey({ armoredKey: requesterKey.publicKeyArmored });
	const requesterPublicKeyFingerprint = tmpRequesterPublicKey.keyPacket.fingerprint.join('');
	
	return {
		key : requesterKey, 
		publicKeyFingerprint : requesterPublicKeyFingerprint
	};
}

async function setupSchlussApp(){

	// User creates vault (app setup) by generating a new keypair:
    const userKey = await openpgp.generateKey({
        type: 'ecc', 
        curve: 'curve25519',
        userIds: [{ name: 'x', email: 'x@x.com' }], 
        passphrase: 'userpassword'
    });
	// also get the public key fingerprint of the user
	// note this must be possible to generate at schluss app side and user gateway side
	const tmpPublicKey = await openpgp.readKey({ armoredKey: userKey.publicKeyArmored });
	const publicKeyFingerprint = tmpPublicKey.keyPacket.fingerprint.join('');

	return {
		key : userKey, 
		publicKeyFingerprint : publicKeyFingerprint
	};
	
}

async function registerExternalStorage(){
	output.message('Registration request:');
	output.message(JSON.stringify({ url : storageApiHost + 'register' }, null, 2));
	
	registerResult = JSON.parse(await request.post(storageApiHost + 'register'));

	output.message('Sending...');
	
	return registerResult;
}
async function authorizeSchlussAppExternalStorage(){
	
	
	
	let authRequest = { url : storageApiHost + 'auth', body : { 'key' : registerResult.key, 'secret' : registerResult.secret }};
	
	//output.message('Auth request:');
	//output.message(JSON.stringify(authRequest, null, 2));	
			
	let authResult = JSON.parse(await request.post(authRequest.url, {
						body : authRequest.body,
						json:true
						}));
	
	return authResult;
}

async function getAndStoreAttributesLocally(user, requester){
	
	// Data request - get attribute
	output.message('\n\nFrom a datarequest, retrieve an attribute from dataprovider...');
	
	
	var attribute = { 
		name : 'lastname', 
		value : 'Janssen', 
	};
	
	output.status('ok');
	
	output.message('Answer: attribute:\n');
	output.message(JSON.stringify(attribute, null, 2));
		
	await output.wait(2);
		
	output.message('\n\nNow store the retrieved attribute in app as follows:');
		
	// Store attribute in app
	let appAttribute = {
		ID : 1,
		version : 1, 	// used to track version globally (when stored in multiple places)
		name : attribute.name,
		label : 'Surname',
		category : 'Persoonsgegevens',
		providerName : 'dataprovider',
		location : ''	// still empty because it is not yet stored online!
		// more meta data here...
	};	
	
	// generate a random attrKey:
	let attrKey = 'thisIsARandomAttributeKey';
	
	// encrypt the attribute value with the attrKey:
	let attrValueEnc = aes256.encrypt(attrKey, attribute.value);
	
	// encrypt attrKey with users' public key:
    const attrkeyEncrypted = await openpgp.encrypt({
        message: openpgp.Message.fromText(attrKey),
        publicKeys: (await openpgp.readKey({ armoredKey: user.key.publicKeyArmored }))
    });
	
	// ecnrypt the attrKey with requesters' public key:
    const attrkeyRequesterEncrypted = await openpgp.encrypt({
        message: openpgp.Message.fromText(attrKey),
        publicKeys: (await openpgp.readKey({ armoredKey: requester.key.publicKeyArmored }))
    });
	
	let keys = {};
	keys[user.publicKeyFingerprint] = attrkeyEncrypted;						// allows user to decrypt value
	keys[requester.publicKeyFingerprint] = attrkeyRequesterEncrypted;	// allows requester to decrypt value
	let appAttributeValue = {
		ID : 1,
		version : 1, 		
		value : attrValueEnc,
		keys : keys
	};
	
	let appAttributeVersion = {
		ID : 1,
		version : 1,
		attributeId : 1,
		attributeValueId : 1,
		timestamp : ''
	};
	
	output.message('attribute:')
	output.message(JSON.stringify(appAttribute, null, 2));
	output.message('attribute version:')
	output.message(JSON.stringify(appAttributeVersion, null, 2));
	output.message('attribute value:')
	output.message(JSON.stringify(appAttributeValue, null, 2));
	
	return {
		attribute : appAttribute,
		attributeVersion : appAttributeVersion,
		attributeValue :appAttributeValue
		
		
	};
}

async function storeAttributesExternalStorage(user, appStorage, extStorageToken){
	
	// Prepare and externally store attribute value
	output.message('Store attribute values and versions first:');
	
	let extAttrValueEnc = {
		version: appStorage.attributeValue.version,
		value : appStorage.attributeValue.value,
		keys : appStorage.attributeValue.keys
	};

	output.message('\nAttribute value to be stored externally is as follows:');
	output.message(JSON.stringify(extAttrValueEnc, null, 2));
	await output.wait(2);
	
	let storeRequest = { url : storageApiHost + 'objects', headers : { 'authorization' : extStorageToken }, body : extAttrValueEnc};
	output.message(JSON.stringify(storeRequest, null, 2));	
				
	let storeResult = JSON.parse(await request.post(storeRequest.url,	{
						body : storeRequest.body,
						headers : storeRequest.headers,
						json:true
						}));
	
	output.message('Sending...');
	await output.wait(1);
	output.status('ok');
	
	output.message('Answer: object location ' + storeResult.location);
	
	// Keep the location of the stored attribute value in memory. Needed at ext stored attribute and datarequest share to data requester
	let attrValueStoreLocation = storeResult.location;
	
	// Prepare and externally store attribute meta data, actually not needed for this datarequest, but needed for the app when it needs to restore the whole attribute (after switching phone for example)
	
	// add additional version information to the attribute to be stored externally
	let appAttributeToStore = appStorage.attributeValue;
	appAttributeToStore.valueVersion = 1;	// must contain latest version
	appAttributeToStore.valueVersions = [{version : 1, location : attrValueStoreLocation}]; // contains the locations of all stored versions
	

	// encrypt attrKey with users' public key:
    const attrEncrypted = await openpgp.encrypt({
        message: openpgp.Message.fromText(JSON.stringify(appAttributeToStore)),
        publicKeys: (await openpgp.readKey({ armoredKey: user.key.publicKeyArmored }))
    });
	
	let extAttrEnc = {
		content : attrEncrypted,
	};
	
	
	await output.wait(1);
	output.message('\nAttribute is encrypted as a whole (only accessible by Schluss user), to be stored as an object at external storage:');
	await output.wait(1);
	
	storeRequest = { url : storageApiHost + 'objects', headers : { 'authorization' : extStorageToken }, body : extAttrEnc};
	output.message(JSON.stringify(storeRequest, null, 2));	
							
	storeResult = JSON.parse(await request.post(storeRequest.url,	{
						body : storeRequest.body,
						headers : storeRequest.headers,
						json:true
						}));
	
	output.message('Sending...');
	await output.wait(1);
	output.status('ok');
	
	output.message('Answer: object location ' + storeResult.location);
	
	// Update local stored attribute with location (so we know there is an online version and where it is!)
	appStorage.attribute.location = storeResult.location;
	
	// we need the attribute value externally store location later on
	return attrValueStoreLocation;
		
}

async function createUserGatewayLink(attrValueStoreLocation, extStorageToken){
	let gatewayStoreRequest = { url: gatewayApiHost + 'links', body : {provider : 'schluss', location : attrValueStoreLocation, headers : { authorization : extStorageToken}}};
	
	await output.wait(4);
	output.message("Gateway store request:");
	output.message(JSON.stringify(gatewayStoreRequest, null, 2));
		
	gatewayResult = JSON.parse(await request.post(gatewayStoreRequest.url,	{
						body : gatewayStoreRequest.body,
						json:true
						}));
						
	output.message("Sending...");
	await output.wait(1);
	output.status('ok');
	
	output.message('Answer from gateway:');
	output.message(JSON.stringify(gatewayResult, null, 2));
	
	return gatewayResult;
}

async function datarequesterGetDataFromUserGateway(gatewayResult, requester){
	let gatewayRequest = { url : gatewayResult.location, headers : {  }, body : { pubkey : requester.key.publicKeyArmored,  purpose : "Getting required information for mortgage" }};

	output.message('Datarequester requests to gateway:');
	output.message(JSON.stringify(gatewayRequest, null, 2));
	
	requesterResult = JSON.parse(await request.post(gatewayRequest.url,	{
						body : gatewayRequest.body,
						headers : gatewayRequest.headers,
						json:true
						}));

	output.message("Sending...");
	await output.wait(1);
	output.status('ok');

	output.message("Answer from Gateway to datarequester:\n");

	output.message(JSON.stringify(requesterResult, null, 2));
	
	await output.wait(2);
	output.message("\nDecrypt attribute value with private key from datarequester, result:\n\n");
	
    const message = await openpgp.readMessage({
        armoredMessage: requesterResult.key // parse armored message
    });	
	
    const privateKey = await openpgp.readKey({ armoredKey: requester.key.privateKeyArmored });
    await privateKey.decrypt('providerpassword');	
	
    const { data: decrypted } = await openpgp.decrypt({
        message,
        publicKeys: (await openpgp.readKey({ armoredKey: requester.key.publicKeyArmored })), // for verification (optional)
        privateKeys: privateKey  // for decryption
    });
	
	// decrypt value:
	let finalValue = aes256.decrypt(decrypted, requesterResult.value);
	
    console.log(finalValue);	
}

async function userGetsAccessLog(){
	output.message("\n\nAdditionally, the user looks up the access log for the gateway link\n\n");	
	await output.wait(1);
	
	let logRequest = { url : gatewayApiHost + 'logs/' + gatewayResult.objectId, headers : { Authorization : gatewayResult.token }};

	let logResult = JSON.parse(await request.get(logRequest.url,	{
						headers : logRequest.headers,
						json:true
						}));
			
	output.message("Returned logs from gateway:\n");
	output.message(JSON.stringify(logResult, null, 2));
}



const output = {
	
	// helper functions
	message : function(text)
	{
		process.stdout.write('\n' + text);
	},

	status : function(type, text)
	{
		// Colors for CLI output
		var WHT = '\033[39m';
		var RED = '\033[91m';
		var GRN = '\033[32m';
		
		text == undefined ? '' : text;
		
		switch(type)
		{
			case 'ok' : process.stdout.write(GRN + '[ok]' + WHT);
			break;
			case 'error' : process.stdout.write(RED + '[error]' + WHT);
			break;	
		}	

		console.log('');
	},
	
	wait : async function(seconds)
	{
		return 0;
		//if (config.debug.extraSlowwww)
			//seconds *= 2;
		
		//if (config.debug.enableSleep)
			return sleep(seconds * 1000);
	},	
};

// Run the whole thing
run();


